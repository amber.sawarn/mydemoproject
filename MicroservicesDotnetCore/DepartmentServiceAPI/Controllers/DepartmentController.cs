﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DepartmentServiceAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //get all the departments from DataAccessLayer
            return new List<string> { "Account", "Admin", "Sales" };
        }
    }
}
